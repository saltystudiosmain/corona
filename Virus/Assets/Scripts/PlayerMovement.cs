﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public PlayerController pc;

    public Joystick joystick;

    public float moveSpeed;
    public float jumpHeight;

    private float gravityMultiplier = 2.5f;

    float horizontalMove = 0f;
    float verticalMove = 0f;

    public bool crouch = false;

    Rigidbody body; 

    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        JoyStickCheck();
    }

    private void FixedUpdate()
    {
        if (body.velocity.y <= 0f)
        {
            body.velocity += Vector3.up * Physics.gravity.y * (gravityMultiplier - 1) * Time.fixedDeltaTime;
        }
    }

    //This is where we will make the player jump.
    public void Jump()
    {
        if (pc.isGrounded)
        {
            body.velocity = Vector3.up * jumpHeight;
            pc.isGrounded = false;

        }
    }
    //This is where the player will be moved.
    public void Move()
    {
        Vector3 move = new Vector3(horizontalMove, 0f, verticalMove);

        transform.Translate(move * Time.fixedDeltaTime * moveSpeed);
    }

    public void JoyStickCheck()
    {
        if (joystick.Horizontal >= .2f || Input.GetKey(KeyCode.D))
        {
            horizontalMove = 1f;
        }
        else if (joystick.Horizontal <= -0.2f || Input.GetKey(KeyCode.A))
        {
            horizontalMove = -1f;
        }
        else horizontalMove = 0f;

        if (joystick.Vertical >= 0.2f || Input.GetKey(KeyCode.W))
        {
            verticalMove = 1f;
        }
        else if (joystick.Vertical <= -0.2f || Input.GetKey(KeyCode.S))
        {
            verticalMove = -1f;
        }
        else verticalMove = 0f;

        Move();
    }

}
